﻿angular.module("AngularAuthApp").controller("houseController", ["$scope", "houseService", function ($scope, houseService) {
    $scope.houses = []; 
    $scope.currentHouse = {};
    $scope.realtTypes = [];
    $scope.search = {};
    $scope.search.RoomCount = "";
    $scope.newHouse = {};

    $scope.houseDetails = function(house) {
        $scope.currentHouse = angular.copy(house);
        $("#details").modal("show");
    };

    $scope.setRoomCount = function (count) {
        $scope.search.RoomCount = count;
    };

    $scope.addHouse = function () {
        houseService.addHouse($scope.newHouse)
            .success(function (data) {
                $scope.houses = data;
                $("#myModal").modal("hide");
            }).error(function () {
                $("#myModal").modal("hide");
            });
    };
    houseService.getHouses().then(function (results) {
        houseService.getRealtTypes().then(function (res) {
            $scope.realtTypes = res.data;
            $scope.newHouse.RealtType = $scope.realtTypes[0];
        });
        $scope.houses = results.data;   

    }, function (error) {
        //alert(error.data.message);
    });
}]);