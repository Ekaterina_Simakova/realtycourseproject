﻿"use strict";
angular.module("AngularAuthApp").factory("houseService", ["$http", function ($http) {

    var houseService = {};

    houseService.getHouses = function () {
        return $http.get(serviceBase + 'api/house/getall').then(function (results) {
            return results;
        });
    };
    houseService.getRealtTypes = function () {
        return $http.get(serviceBase + 'api/realt/getall').then(function (results) {
            return results;
        });
    }
    houseService.deleteHouseFeature = function (url, houseId, featureId) {
        return $http({
            url: url,
            method: "GET",
            params: {
                houseId: houseId, 
                featureId: featureId
            }
        });
    }

    houseService.getFeatures = function (url, id) {
        return $http({
            url: url,
            method: "GET",
            params: {
                id: id
            }
        });
    }
    houseService.getUsers = function (url) {
        return $http({
            url: url,
            method: "GET"
        });
    }
    houseService.addHouse = function (data) {
        return $http.post(serviceBase + 'api/house/addhouse', { house: data});
    };
    houseService.addFeature = function (url, id, feature) {
        return $http.post(url, { id: id, featureId: feature });
    };
    houseService.changeHouse = function (url, data) {
        return $http.post(url, { house: data });
    };

    houseService.removeImage = function (url, id, data) {
        return $http.post(url, { url: data, id: id });
    };
    houseService.deleteHouse = function (url, id) {
        return $http({
            url: url,
            method: "GET",
            params: {
                id: id
            }
        });
    };
    houseService.setHouseId = function (url, id) {
        return $http({
            url: url,
            method: "GET",
            params: {
                id: id
            }
        });
    };


    return houseService;
}]);