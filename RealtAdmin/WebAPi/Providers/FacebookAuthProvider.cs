﻿using System.Security.Claims;
using System.Threading.Tasks;
using Duke.Owin.VkontakteMiddleware;
using Duke.Owin.VkontakteMiddleware.Provider;
using Microsoft.Owin.Security.Facebook;

namespace WebAPi.Providers
{
    public class FacebookAuthProvider : FacebookAuthenticationProvider
    {
        public override Task Authenticated(FacebookAuthenticatedContext context)
        {
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            return Task.FromResult<object>(null);
        }
    }

    public class VkAuthProvider : VkAuthenticationProvider    
    {
        public override Task Authenticated(VkAuthenticatedContext context)
        {
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            return Task.FromResult<object>(null);
        }

        public override Task ReturnEndpoint(VkReturnEndpointContext context)
        {
            return Task.FromResult<object>(null);
        }
    }
}