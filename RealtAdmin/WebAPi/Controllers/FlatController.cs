﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using Moodroon.DataLayer.Core;
using Moodroon.DataLayer.Core.DTO;
using Moodroon.DataLayer.Core.Services;

namespace WebAPi.Controllers
{
    [Authorize]
    [RoutePrefix("api/flat")]
    public class FlatController : ApiController
    {
        private readonly FlatService _flatService;

        public FlatController()
        {
            this._flatService = new FlatService(UnityFactory.Instance.Container);
        }

        [HttpGet]
        [Route("getall")]
        [ResponseType(typeof(List<FlatDto>))]
        public IHttpActionResult GetAllFlat()   
        {
            return this.Ok(this._flatService.GetFlats());
        }

        [HttpPost]
        [Route("addflat")]
        [ResponseType(typeof(List<FlatDto>))]
        public IHttpActionResult AddFlat(FlatDto flat)
        {
            this._flatService.AddFlat(flat);
            return this.Ok(this._flatService.GetFlats());
        }
    }
}
