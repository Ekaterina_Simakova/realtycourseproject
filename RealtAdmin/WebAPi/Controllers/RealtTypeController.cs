﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using Moodroon.DataLayer.Core;
using Moodroon.DataLayer.Core.DTO;
using Moodroon.DataLayer.Core.Services;

namespace WebAPi.Controllers
{
    [System.Web.Http.Authorize]
    [System.Web.Http.RoutePrefix("api/realt")]
    public class RealtTypeController : ApiController
    {
        private readonly RealtTypeService _realtTypeService;

        public RealtTypeController()
        {
            this._realtTypeService = new RealtTypeService(UnityFactory.Instance.Container);
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("getall")]
        [ResponseType(typeof(List<RealtTypeDto>))]
        public IHttpActionResult GetRealtTypes()
        {
            return this.Ok(this._realtTypeService.GetRealtTypes());
        }

    }
}