﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using Moodroon.DataLayer.Core;
using Moodroon.DataLayer.Core.DTO;
using Moodroon.DataLayer.Core.Services;

namespace WebAPi.Controllers
{
    [Authorize]
    [RoutePrefix("api/house")]
    public class HouseController : ApiController
    {
        private readonly HouseService _houseService;

        public HouseController()
        {
            this._houseService = new HouseService(UnityFactory.Instance.Container);
        }

        [HttpGet]
        [Route("getall")]
        [ResponseType(typeof(List<HouseDto>))]
        public IHttpActionResult GetAllHouse()    
        {
            return this.Ok(this._houseService.GetHouses());
        }

        [HttpPost]
        [Route("addHouse")]
        [ResponseType(typeof(List<HouseDto>))]
        public IHttpActionResult AddHouse(HouseDto house) 
        {
            this._houseService.AddUserHouse(house,Convert.ToInt32(this.User.Identity.GetUserId()));
            return this.Ok(this._houseService.GetHouses());
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
