﻿namespace DataAccessLayer.Models.Models
{
    using System.Collections.Generic;

    public class RealtType
    {
        public RealtType()
        {
            this.Flats = new HashSet<Flat>();
            this.Houses = new HashSet<House>();
        }
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Flat> Flats { get; set; }

        public virtual ICollection<House> Houses { get; set; }
    }
}
