﻿namespace DataAccessLayer.Models.Models
{
    using System.Collections.Generic;

    public class Flat
    {
        public Flat()
        {
            this.Images = new HashSet<Image>();
            this.Featureses=new HashSet<Features>();
        }
        public int Id { get; set; }

        public int CountRooms { get; set; }

        public int TotalSquare { get; set; }

        public string Adress { get; set; }

        public string Description { get; set; }

        public int IdRealtType { get; set; }

        public int Price { get; set; }

        public int IdUser { get; set; } 

        public virtual ICollection<Image> Images { get; set; }

        public virtual ICollection<Features> Featureses { get; set; }

        public virtual User User{ get; set; }

        public virtual RealtType RealtType { get; set; }
    }
}
