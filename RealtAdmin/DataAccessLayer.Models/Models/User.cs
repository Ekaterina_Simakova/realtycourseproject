﻿namespace DataAccessLayer.Models.Models
{
    using System.Collections.Generic;

    using Microsoft.AspNet.Identity.EntityFramework;

    public class User : IdentityUser<int, UserLogin, UserRole, UserClaim>
    {
        public User()
        {
            this.Flats = new HashSet<Flat>();
        }

        public virtual ICollection<Flat> Flats { get; set; }

        public virtual ICollection<House> Houses { get; set; }  
    }
}
