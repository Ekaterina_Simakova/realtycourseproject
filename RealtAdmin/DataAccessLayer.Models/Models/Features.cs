﻿using System.Collections.Generic;

namespace DataAccessLayer.Models.Models
{
    using System;

    public class Features
    {
        public Features()
        {
            this.Flats = new HashSet<Flat>();

            this.Houses = new HashSet<House>();
        }
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Flat> Flats { get; set; }

        public virtual ICollection<House> Houses { get; set; }
    }
}
