﻿namespace DataAccessLayer.Models.Models
{
    public class Reservation
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int IdHouse { get; set; }

        public int IdFlat { get; set; } 
    }
}
