﻿namespace DataAccessLayer.Models.Models
{
    using System.Collections.Generic;

    public class Image
    {
        public Image()
        {
            this.Flats = new HashSet<Flat>();
            this.Houses = new HashSet<House>();
        }
        public int Id { get; set; }

        public string Url { get; set; }

        public virtual ICollection<Flat> Flats { get; set; }

        public virtual ICollection<House> Houses { get; set; }
    }
}
