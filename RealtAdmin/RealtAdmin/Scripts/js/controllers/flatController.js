﻿angular.module("yapp").controller("flatController", ["$scope", "flatService", function ($scope, flatService) {
    $scope.flats = [];
    $scope.currentFlat = {};
    $scope.realtTypes = [];
    $scope.newFlat = {};
    $scope.users = {};
    $scope.features = {};

    $scope.getRealtTypes = function (url, usersUrl) {
        flatService.getRealtTypes(url)
           .success(function (data) {
               $scope.realtTypes = data;
               $scope.newFlat.RealtType = $scope.realtTypes[0];
               $scope.currentFlat.RealtType = $scope.realtTypes[0];
               $scope.getUsers(usersUrl);
           })
           .error(function () {
           });
    }
    $scope.getFeatures = function (url) {    
        flatService.getFeatures(url, $scope.currentFlat.Id) 
           .success(function (data) {
               $scope.features = data;
           })
           .error(function () {
           });
    }
    $scope.getUsers = function (url) {
        flatService.getUsers(url)
           .success(function (data) {
               $scope.users = data;
               $scope.newFlat.User = $scope.users[0];
               $scope.currentFlat.User = $scope.users[0];
           })
           .error(function () {
           });
    }

    $scope.addFeature = function (url, feature) {
        flatService.addFeature(url, $scope.currentFlat.Id, feature.Id) 
           .success(function (data) {
               $scope.features.splice($scope.features.indexOf(feature), 1);
                $scope.getFlats();
            })
           .error(function () {
           });
    }
    $scope.deleteFlatFeature = function (url, flat,feature) {        
        flatService.deleteFlatFeature(url, flat.Id, feature.Id)
           .success(function (data) {
               flat.FeaturesDtos.splice(flat.FeaturesDtos.indexOf(feature), 1);
           })
           .error(function () {
           });
    }
    $scope.seeFlatImage = function (flat) {
        $scope.currentFlat = angular.copy(flat);
        $("#imagesModal").modal("show");
    };
    $scope.setFlat = function (flat) {  
        $scope.currentFlat = angular.copy(flat);
    };
    $scope.addFeatureSetFlat = function (url,flat) {
        $scope.currentFlat = angular.copy(flat);
        $scope.getFeatures(url);
        $("#addFeature").modal("show"); 
    };

    $scope.editFlat = function (url, usersUrl, flat) {
        $scope.currentFlat = angular.copy(flat);
        $scope.getRealtTypes(url, usersUrl);
        $("#editModal").modal("show");
    };



    $scope.changeFlat = function (url) {
        flatService.changeFlat(url, $scope.currentFlat) 
            .success(function () {
                $scope.getFlats();
                $("#editModal").modal("hide");  
            }).error(function () {
                $("#editModal").modal("hide");
                $("#hotelExistError").show();
                window.setTimeout(function () {
                    $("#hotelExistError").fadeTo(500, 0).slideUp(500, function () {
                        $(this).remove();
                    });
                }, 3000);
            });
    };

    $scope.removeImage = function (url, image) {
        flatService.removeImage(url, $scope.currentFlat.Id, image)
            .success(function (data) {
                $scope.currentFlat.ImageDtos.splice($scope.currentFlat.ImageDtos.indexOf(image), 1);    
                $scope.getFlats();
            })
            .error(function () {
            });
    };

    $scope.uploadImage = function (url, flatId) {
        flatService.setHotelId(url, flatId)
            .success(function (data) {
            })
            .error(function () {
            });
    };
    $scope.init = function (url) {
        $scope.getFlatsUrl = url;
        $scope.getFlats();
    };
    $scope.getFlats = function () {
        flatService.getFlats($scope.getFlatsUrl)
           .success(function (data) {
               $scope.flats = data;
           }).error(function () {

           });
    }
    $scope.deleteFlat = function (url, flat) {
        flatService.deleteFlat(url, flat.Id)
            .success(function (data) {
                $scope.flats.splice($scope.flats.indexOf(flat), 1);
            }).error(function () {

            });
    };

    $scope.addFlat = function (url) {
        flatService.addFlat(url, $scope.newFlat)
            .success(function (data) {
                $scope.flats = data;
                $("#myModal").modal("hide");
            }).error(function () {
                $("#myModal").modal("hide");
                $("#hotelExistError").show();
                window.setTimeout(function () {
                    $("#hotelExistError").fadeTo(500, 0).slideUp(500, function () {
                        $(this).remove();
                    });
                }, 3000);
            });
    };
}]);