﻿angular.module("yapp").controller("featuresController", ["$scope", "featuresService", function ($scope, featuresService) {

    $scope.features = [];
    $scope.currentFeature = {};
    $scope.newFeature = {};    
    $scope.getFeaturesUrl = "";    

    $scope.editFeature = function (feature) {   
        $scope.currentFeature = angular.copy(feature);  
        $("#editModal").modal("show");  
    };
     $scope.init = function (url) {
        $scope.getFeaturesUrl = url;    
        $scope.getFeatures();
    };
     $scope.getFeatures = function () {
        featuresService.getFeatures($scope.getFeaturesUrl)
          .success(function (data) {
              $scope.features = data;
          }).error(function () {

          });
     };
     $scope.addFeature = function (url) {   
         featuresService.addFeature(url, $scope.newFeature) 
           .success(function (data) {
               $scope.features = data;
           }).error(function () {
               $("#myModal").modal("hide");
               $("#FeatureExist").show();   
               window.setTimeout(function () {
                   $("#FeatureExist").fadeTo(500, 0).slideUp(500, function () {
                       $(this).remove();
                   });
               }, 3000);
           });
     };
     
    $scope.saveEditFeature = function(url) {    
        featuresService.saveEditFeature(url, $scope.currentFeature)         
            .success(function (data) {
                $("#editModal").modal("hide");  
                $scope.getFeatures();
            })
            .error(function (data) {
                $("#editModal").modal("hide");  
                $("#FeatureExist").show();
                window.setTimeout(function () {
                    $("#FeatureExist").fadeTo(500, 0).slideUp(500, function () {
                        $(this).remove();
                    });
                }, 3000);   
            });
    };

   

    $scope.deleteFeature = function (url, feature) {
        featuresService.deleteFeature(url, feature.Id)  
          .success(function (data) {
              $scope.features.splice($scope.features.indexOf(feature), 1);  
          }).error(function () {

          });
    };
   
}]);