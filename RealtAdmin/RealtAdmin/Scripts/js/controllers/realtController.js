﻿angular.module("yapp").controller("realtController", ["$scope", "realtService", function ($scope, realtService) {

    $scope.realts = []; 
    $scope.currentRealt = {};   
    $scope.newRealt = {};    
    $scope.getRealtUrl = "";           

    $scope.editRealt = function (realt) {     
        $scope.currentRealt = angular.copy(realt);  
        $("#editModal").modal("show");  
    };
     $scope.init = function (url) {
        $scope.getRealtUrl = url;    
        $scope.getRealts();  
    };
     $scope.getRealts = function () {
        realtService.getRealts($scope.getRealtUrl)
          .success(function (data) {
              $scope.realts = data; 
          }).error(function () {

          });
     };
     $scope.addRealt = function (url) {   
        realtService.addRealt(url, $scope.newRealt)      
           .success(function (data) {
               $scope.realts = data;    
               $("#myModal").modal("hide");
           }).error(function () {
               $("#myModal").modal("hide");
               $("#realtExist").show();   
               window.setTimeout(function () {
                   $("#realtExist").fadeTo(500, 0).slideUp(500, function () {
                       $(this).remove();
                   });
               }, 3000);
           });
     };
     
    $scope.saveEditRealt = function(url) {          
        realtService.saveEditRealt(url, $scope.currentRealt)            
            .success(function (data) {
                $("#editModal").modal("hide");  
                $scope.getRealts();  
            })
            .error(function (data) {
                $("#editModal").modal("hide");  
                $("#realtExist").show();
                window.setTimeout(function () {
                    $("#realtExist").fadeTo(500, 0).slideUp(500, function () {
                        $(this).remove();
                    });
                }, 3000);   
            });
    };

   

    $scope.deleteRealt = function (url, realt) {
        realtService.deleteRealt(url, realt.Id)  
          .success(function (data) {
              $scope.realts.splice($scope.realts.indexOf(realt), 1);    
          }).error(function () {

          });
    };
   
}]);