﻿angular.module("yapp").controller("houseController", ["$scope", "houseService", function ($scope, houseService) {
    $scope.houses = []; 
    $scope.currentHouse = {};
    $scope.realtTypes = [];
    $scope.newHouse = {};
    $scope.users = {};
    $scope.features = {};

    $scope.getRealtTypes = function (url, usersUrl) {
        houseService.getRealtTypes(url)
           .success(function (data) {
               $scope.realtTypes = data;
               $scope.newHouse.RealtType = $scope.realtTypes[0];
               $scope.currentHouse.RealtType = $scope.realtTypes[0];
               $scope.getUsers(usersUrl);
           })
           .error(function () {
           });
    }
    $scope.getFeatures = function (url) {    
        houseService.getFeatures(url, $scope.currentHouse.Id) 
           .success(function (data) {
               $scope.features = data;
           })
           .error(function () {
           });
    }
    $scope.getUsers = function (url) {
        houseService.getUsers(url)  
           .success(function (data) {
               $scope.users = data;
               $scope.newHouse.User = $scope.users[0];
               $scope.currentHouse.User = $scope.users[0];
           })
           .error(function () {
           });
    }

    $scope.addFeature = function (url, feature) {
        houseService.addFeature(url, $scope.currentHouse.Id, feature.Id) 
           .success(function (data) {
               $scope.features.splice($scope.features.indexOf(feature), 1);
                $scope.getHouses();
            })
           .error(function () {
           });
    }
    $scope.deleteHouseFeature = function (url, house,feature) {        
        houseService.deleteHouseFeature(url, house.Id, feature.Id)
           .success(function (data) {
               house.FeaturesDtos.splice(house.FeaturesDtos.indexOf(feature), 1);
           })
           .error(function () {
           });
    }
    $scope.seeHouseImage = function (house) {
        $scope.currentHouse = angular.copy(house);
        $("#imagesModal").modal("show");
    };
    $scope.setHouse = function (house) {  
        $scope.currentHouse = angular.copy(house);
    };
    $scope.addFeatureSetHouse = function (url,house) {
        $scope.currentHouse = angular.copy(house);
        $scope.getFeatures(url);
        $("#addFeature").modal("show"); 
    };

    $scope.editHouse = function (url, usersUrl, house) {
        $scope.currentHouse = angular.copy(house);
        $scope.getRealtTypes(url, usersUrl);
        $("#editModal").modal("show");
    };



    $scope.changeHouse = function (url) {
        houseService.changeHouse(url, $scope.currentHouse) 
            .success(function () {
                $scope.getHouses();
                $("#editModal").modal("hide");  
            }).error(function () {
                $("#editModal").modal("hide");
                $("#hotelExistError").show();
                window.setTimeout(function () {
                    $("#hotelExistError").fadeTo(500, 0).slideUp(500, function () {
                        $(this).remove();
                    });
                }, 3000);
            });
    };

    $scope.removeImage = function (url, image) {
        houseService.removeImage(url, $scope.currentHouse.Id, image)
            .success(function (data) {
                $scope.currentHouse.ImageDtos.splice($scope.currentHouse.ImageDtos.indexOf(image), 1);    
                $scope.getHouses();
            })
            .error(function () {
            });
    };

    $scope.uploadImage = function (url, houseId) {
        houseService.setHotelId(url, houseId)
            .success(function (data) {
            })
            .error(function () {
            });
    };
    $scope.init = function (url) {
        $scope.getHousesUrl = url;
        $scope.getHouses();
    };
    $scope.getHouses = function () {
        houseService.getHouses($scope.getHousesUrl)
           .success(function (data) {
               $scope.houses = data;    
           }).error(function () {

           });
    }
    $scope.deleteHouse = function (url, house) {
        houseService.deleteHouse(url, house.Id)
            .success(function (data) {
                $scope.houses.splice($scope.houses.indexOf(house), 1);  
            }).error(function () {

            });
    };

    $scope.addHouse = function (url) {
        houseService.addHouse(url, $scope.newHouse)
            .success(function (data) {
                $scope.houses = data;   
                $("#myModal").modal("hide");
            }).error(function () {
                $("#myModal").modal("hide");
                $("#hotelExistError").show();
                window.setTimeout(function () {
                    $("#hotelExistError").fadeTo(500, 0).slideUp(500, function () {
                        $(this).remove();
                    });
                }, 3000);
            });
    };
}]);