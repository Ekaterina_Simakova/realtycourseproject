﻿"use strict";
angular.module("yapp").factory("realtService", ["$http", function ($http) {

    var realtService = {};

    realtService.getRealts = function (url) {   
        return $http({
            url: url,
            method: "GET"
        });
    };

     realtService.deleteRealt = function (url, id) {     
        return $http({
            url: url,
            method: "GET",
            params: {
                id: id
            }
        });
    };
     realtService.saveEditRealt  = function (url, data) {
         return $http.post(url, { realt: data });
     };

     realtService.addRealt  = function (url, data) {        
         return $http.post(url, { realt: data });   
     };
     
    return realtService;
}]);