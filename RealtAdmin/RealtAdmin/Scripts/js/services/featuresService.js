﻿"use strict";
angular.module("yapp").factory("featuresService", ["$http", function ($http) {

    var featuresService = {};

    featuresService.getFeatures = function (url) {
        return $http({
            url: url,
            method: "GET"
        });
    };

     featuresService.deleteFeature = function (url, id) {   
        return $http({
            url: url,
            method: "GET",
            params: {
                id: id
            }
        });
    };
     featuresService.saveEditFeature = function (url, data) {
         return $http.post(url, { feature: data });
     };

     featuresService.addFeature = function (url, data) {    
         return $http.post(url, { feature: data }); 
     };
     
    return featuresService;
}]);