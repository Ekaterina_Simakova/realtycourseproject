﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Amazon.S3;
using Amazon.S3.Model;
using Moodroon.DataLayer.Core;
using Moodroon.DataLayer.Core.DTO;
using Moodroon.DataLayer.Core.Services;

namespace RealtAdmin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class HouseController : Controller
    {
        private readonly HouseService _houseService;    

        public HouseController()
        {
            this._houseService = new HouseService(UnityFactory.Instance.Container);
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetHouses()
        {
            return this.Json(this._houseService.GetHouses(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteHouse(int id)
        {
            this._houseService.DeleteHouse(id);
            return this.Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddHouse(HouseDto house)
        {
            this._houseService.AddHouse(house);
            return this.Json(this._houseService.GetHouses(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditHouse(HouseDto house)
        {
            this._houseService.EditHouse(house);
            return this.Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddFeature(int id, int featureId)
        {
            this._houseService.AddIFeature(id, featureId);
            return this.Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteImage(int id, string url)
        {
            this._houseService.DeleteImage(id, url);
            return this.Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public RedirectToRouteResult UploadImage(HttpPostedFileBase file, int id)
        {
            if (file != null)
            {

                var s3Config = new Amazon.S3.AmazonS3Config { ServiceURL = AppConfiguration.AppConfiguration.Instance.ServiceUrl };
                using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(
                    AppConfiguration.AppConfiguration.Instance.AccessKey,
                   AppConfiguration.AppConfiguration.Instance.SecretKey,
                   s3Config))
                {
                    var request = new PutObjectRequest
                    {
                        BucketName = "imagesforhotel",
                        Key = file.FileName,
                        InputStream = file.InputStream,
                        CannedACL = S3CannedACL.PublicRead
                    };
                    client.PutObject(request);
                    var url = "https://s3.amazonaws.com/" + request.BucketName + "/" + file.FileName;
                    this._houseService.AddImage(id, url);
                }
            }
            return this.RedirectToAction("Index");
        }

    }
}