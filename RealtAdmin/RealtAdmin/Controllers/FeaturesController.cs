﻿using System.Web.Mvc;
using Moodroon.DataLayer.Core;
using Moodroon.DataLayer.Core.DTO;
using Moodroon.DataLayer.Core.Services;

namespace RealtAdmin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class FeaturesController : Controller
    {
        private readonly FeaturesService _featuresService;

        public FeaturesController()
        {
            this._featuresService = new FeaturesService(UnityFactory.Instance.Container);
        }
        [HttpGet]
        public ActionResult Index()
        {
            return this.View();
        }

        [HttpGet]
        public JsonResult GetFeatures()
        {
            return this.Json(this._featuresService.GetFeatures(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteFeature(int id)
        {
            this._featuresService.DeleteFeature(id);
            return this.Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddFeature(FeaturesDto feature)    
        {
            if (!this._featuresService.AddFeature(feature))
            {
                return new HttpStatusCodeResult(500);
            }
            return this.Json(this._featuresService.GetFeatures(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditFeature(FeaturesDto feature)    
        {
            if (!this._featuresService.EditFeature(feature))
            {
                return new HttpStatusCodeResult(500);
            }
            return this.Json(this._featuresService.GetFeatures(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetFlatFeatures(int id)   
        {
            return this.Json(this._featuresService.GetFlatFeatures(id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteFlatFeatures(int flatId,int featureId)
        {
            this._featuresService.DeleteFlatFeature(flatId,featureId);
            return this.Json("Succeess", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetHouseFeatures(int id)
        {
            return this.Json(this._featuresService.GetHouseFeatures(id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteHouseFeatures(int houseId, int featureId)    
        {
            this._featuresService.DeleteHouseFeature(houseId, featureId);
            return this.Json("Succeess", JsonRequestBehavior.AllowGet);
        }
    }
}