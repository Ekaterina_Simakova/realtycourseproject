﻿using System.Web;
using System.Web.Mvc;
using Amazon.S3;
using Amazon.S3.Model;
using Moodroon.DataLayer.Core;
using Moodroon.DataLayer.Core.DTO;
using Moodroon.DataLayer.Core.Services;

namespace RealtAdmin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class FlatController : Controller
    {
        private readonly FlatService _flatService;

        public FlatController()
        {
            this._flatService = new FlatService(UnityFactory.Instance.Container);
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetFlats()
        {
            return this.Json(this._flatService.GetFlats(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteFlat(int id)
        {
            this._flatService.DeleteFlat(id);
            return this.Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddFlat(FlatDto flat)
        {
            this._flatService.AddFlat(flat);
            return this.Json(this._flatService.GetFlats(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditFlat(FlatDto flat)
        {
            this._flatService.EditFlat(flat);
            return this.Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddFeature(int id, int featureId)
        {
            this._flatService.AddIFeature(id, featureId);
            return this.Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteImage(int id, string url)    
        {
            this._flatService.DeleteImage(id, url);
            return this.Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public RedirectToRouteResult UploadImage(HttpPostedFileBase file,int id)    
        {
            if (file != null)
            {

                var s3Config = new Amazon.S3.AmazonS3Config { ServiceURL = AppConfiguration.AppConfiguration.Instance.ServiceUrl };
                using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(
                    AppConfiguration.AppConfiguration.Instance.AccessKey,
                   AppConfiguration.AppConfiguration.Instance.SecretKey,
                   s3Config))
                {
                    var request = new PutObjectRequest
                    {
                        BucketName = "imagesforhotel",
                        Key = file.FileName,
                        InputStream = file.InputStream,
                        CannedACL = S3CannedACL.PublicRead
                    };
                    client.PutObject(request);
                    var url = "https://s3.amazonaws.com/" + request.BucketName + "/" + file.FileName;
                    this._flatService.AddImage(id, url);
                }
            }
            return this.RedirectToAction("Index");
        }
    }
}

