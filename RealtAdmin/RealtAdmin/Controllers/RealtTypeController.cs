﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Moodroon.DataLayer.Core;
using Moodroon.DataLayer.Core.DTO;
using Moodroon.DataLayer.Core.Services;

namespace RealtAdmin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RealtTypeController : Controller
    {
        private readonly RealtTypeService _realtTypeService;

        public RealtTypeController()
        {
            this._realtTypeService = new RealtTypeService(UnityFactory.Instance.Container);
        }

        [HttpGet]
        public ActionResult Index()
        {
            return this.View();
        }

        [HttpGet]
        public JsonResult GetRealtTypes()
        {
            return this.Json(this._realtTypeService.GetRealtTypes(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteFeature(int id)
        {
            this._realtTypeService.DeleteRealt(id);
            return this.Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddFeature(RealtTypeDto realt)
        {
            if (!this._realtTypeService.AddRealt(realt))
            {
                return new HttpStatusCodeResult(500);
            }
            return this.Json(this._realtTypeService.GetRealtTypes(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditRealt(RealtTypeDto realt)   
        {
            if (!this._realtTypeService.EditRealt(realt))
            {
                return new HttpStatusCodeResult(500);
            }
            return this.Json(this._realtTypeService.GetRealtTypes(), JsonRequestBehavior.AllowGet);
        }
    }
}