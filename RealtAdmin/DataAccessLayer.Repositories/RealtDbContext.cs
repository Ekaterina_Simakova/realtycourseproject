﻿namespace DataAccessLayer.Repositories
{
    using System.Data.Entity;
    using System.Text.RegularExpressions;

    using DataAccessLayer.Models.Models;
    using DataAccessLayer.Repositories.Interfaces;
    using DataAccessLayer.Repositories.Mapping;

    using Microsoft.AspNet.Identity.EntityFramework;

    public class RealtDbContext : IdentityDbContext<User, Role, int, UserLogin, UserRole, UserClaim>, IRealtDbContext
    {
        public RealtDbContext()
              : base("name=Realt")
        {
        }

        public static RealtDbContext Create()
        {
            return new RealtDbContext();
        }
        
        public IDbSet<Features> Featureses { get; set; }
        public IDbSet<Flat> Flats { get; set; }
        public IDbSet<Image> Images { get; set; }
        public IDbSet<House> Houses { get; set; }
        public IDbSet<RealtType> RealtTypes { get; set; }

        public IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            this.Configuration.LazyLoadingEnabled = false;
            modelBuilder.Configurations.Add(new ImageMap());
            modelBuilder.Configurations.Add(new HouseMap());
            modelBuilder.Configurations.Add(new RealtTypeMap());
            modelBuilder.Configurations.Add(new FeaturesMap());
            modelBuilder.Configurations.Add(new FlatMap());
            modelBuilder.Configurations.Add(new UserMap());

            modelBuilder.Entity<UserRole>().ToTable("UserRoles", "Security").HasKey(p => new { p.UserId, p.RoleId });
            modelBuilder.Entity<UserLogin>().ToTable("UserLogins", "Security").HasKey(p => p.Id);
            modelBuilder.Entity<UserClaim>().ToTable("UserClaims", "Security");
            modelBuilder.Entity<Role>().ToTable("Roles", "Security");
        }

        public new void SaveChanges()
        {
            base.SaveChanges();
        }
    }
}
