﻿namespace DataAccessLayer.Repositories.Interfaces
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Text.RegularExpressions;

    using DataAccessLayer.Models.Models;

    public interface IRealtDbContext : IDisposable  
    {
        IDbSet<Features> Featureses { get; set; }
        IDbSet<Flat> Flats { get; set; }   
        IDbSet<Image> Images { get; set; }  
        IDbSet<House> Houses { get; set; }
        IDbSet<RealtType> RealtTypes { get; set; }

        IDbSet<TEntity> Set<TEntity>() where TEntity : class;

        DbEntityEntry Entry(object entity);

        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        void SaveChanges();

    }
}
