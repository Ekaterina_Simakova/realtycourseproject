namespace DataAccessLayer.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Features",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 60),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Flat",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CountRooms = c.Int(nullable: false),
                        TotalSquare = c.Int(nullable: false),
                        Adress = c.String(),
                        Description = c.String(),
                        IdRealtType = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        IdUser = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RealtType", t => t.IdRealtType, cascadeDelete: true)
                .ForeignKey("Security.Users", t => t.IdUser, cascadeDelete: true)
                .Index(t => t.IdRealtType)
                .Index(t => t.IdUser);
            
            CreateTable(
                "dbo.Image",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.House",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CountRooms = c.Int(nullable: false),
                        TotalSquare = c.Int(nullable: false),
                        Adress = c.String(),
                        Description = c.String(),
                        IdUser = c.Int(nullable: false),
                        IdRealtType = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RealtType", t => t.IdRealtType, cascadeDelete: true)
                .ForeignKey("Security.Users", t => t.IdUser, cascadeDelete: true)
                .Index(t => t.IdUser)
                .Index(t => t.IdRealtType);
            
            CreateTable(
                "dbo.RealtType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Security.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Security.UserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Security.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "Security.UserLogins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LoginProvider = c.String(),
                        ProviderKey = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Security.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "Security.UserRoles",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("Security.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("Security.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "Security.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.House_Image",
                c => new
                    {
                        HouseId = c.Int(nullable: false),
                        ImageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.HouseId, t.ImageId })
                .ForeignKey("dbo.House", t => t.HouseId, cascadeDelete: true)
                .ForeignKey("dbo.Image", t => t.ImageId, cascadeDelete: true)
                .Index(t => t.HouseId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "dbo.Flat_Image",
                c => new
                    {
                        FlatId = c.Int(nullable: false),
                        ImageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FlatId, t.ImageId })
                .ForeignKey("dbo.Flat", t => t.FlatId, cascadeDelete: true)
                .ForeignKey("dbo.Image", t => t.ImageId, cascadeDelete: true)
                .Index(t => t.FlatId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "dbo.Featurese_Flat",
                c => new
                    {
                        FeatureseId = c.Int(nullable: false),
                        FlatId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FeatureseId, t.FlatId })
                .ForeignKey("dbo.Features", t => t.FeatureseId, cascadeDelete: true)
                .ForeignKey("dbo.Flat", t => t.FlatId, cascadeDelete: true)
                .Index(t => t.FeatureseId)
                .Index(t => t.FlatId);
            
            CreateTable(
                "dbo.Featurese_House",
                c => new
                    {
                        FeatureseId = c.Int(nullable: false),
                        HouseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FeatureseId, t.HouseId })
                .ForeignKey("dbo.Features", t => t.FeatureseId, cascadeDelete: true)
                .ForeignKey("dbo.House", t => t.HouseId, cascadeDelete: true)
                .Index(t => t.FeatureseId)
                .Index(t => t.HouseId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Security.UserRoles", "RoleId", "Security.Roles");
            DropForeignKey("dbo.Featurese_House", "HouseId", "dbo.House");
            DropForeignKey("dbo.Featurese_House", "FeatureseId", "dbo.Features");
            DropForeignKey("dbo.Featurese_Flat", "FlatId", "dbo.Flat");
            DropForeignKey("dbo.Featurese_Flat", "FeatureseId", "dbo.Features");
            DropForeignKey("dbo.Flat", "IdUser", "Security.Users");
            DropForeignKey("dbo.Flat", "IdRealtType", "dbo.RealtType");
            DropForeignKey("dbo.Flat_Image", "ImageId", "dbo.Image");
            DropForeignKey("dbo.Flat_Image", "FlatId", "dbo.Flat");
            DropForeignKey("dbo.House", "IdUser", "Security.Users");
            DropForeignKey("Security.UserRoles", "UserId", "Security.Users");
            DropForeignKey("Security.UserLogins", "UserId", "Security.Users");
            DropForeignKey("Security.UserClaims", "UserId", "Security.Users");
            DropForeignKey("dbo.House", "IdRealtType", "dbo.RealtType");
            DropForeignKey("dbo.House_Image", "ImageId", "dbo.Image");
            DropForeignKey("dbo.House_Image", "HouseId", "dbo.House");
            DropIndex("dbo.Featurese_House", new[] { "HouseId" });
            DropIndex("dbo.Featurese_House", new[] { "FeatureseId" });
            DropIndex("dbo.Featurese_Flat", new[] { "FlatId" });
            DropIndex("dbo.Featurese_Flat", new[] { "FeatureseId" });
            DropIndex("dbo.Flat_Image", new[] { "ImageId" });
            DropIndex("dbo.Flat_Image", new[] { "FlatId" });
            DropIndex("dbo.House_Image", new[] { "ImageId" });
            DropIndex("dbo.House_Image", new[] { "HouseId" });
            DropIndex("Security.UserRoles", new[] { "RoleId" });
            DropIndex("Security.UserRoles", new[] { "UserId" });
            DropIndex("Security.UserLogins", new[] { "UserId" });
            DropIndex("Security.UserClaims", new[] { "UserId" });
            DropIndex("dbo.House", new[] { "IdRealtType" });
            DropIndex("dbo.House", new[] { "IdUser" });
            DropIndex("dbo.Flat", new[] { "IdUser" });
            DropIndex("dbo.Flat", new[] { "IdRealtType" });
            DropTable("dbo.Featurese_House");
            DropTable("dbo.Featurese_Flat");
            DropTable("dbo.Flat_Image");
            DropTable("dbo.House_Image");
            DropTable("Security.Roles");
            DropTable("Security.UserRoles");
            DropTable("Security.UserLogins");
            DropTable("Security.UserClaims");
            DropTable("Security.Users");
            DropTable("dbo.RealtType");
            DropTable("dbo.House");
            DropTable("dbo.Image");
            DropTable("dbo.Flat");
            DropTable("dbo.Features");
        }
    }
}
