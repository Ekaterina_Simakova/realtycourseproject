﻿namespace DataAccessLayer.Repositories.Repositories.DataBaseRepositories
{
    using DataAccessLayer.Models.Models;
    using DataAccessLayer.Repositories.Interfaces;
    using DataAccessLayer.Repositories.Repositories;

    public class RealTypeRepository : BaseRepository<RealtType>
    {
        public RealTypeRepository(IRealtDbContext dataContext)
            : base(dataContext)
        {
        }
    }
}
