﻿namespace DataAccessLayer.Repositories.Repositories.DataBaseRepositories
{
    using DataAccessLayer.Models.Models;
    using DataAccessLayer.Repositories.Interfaces;
    using DataAccessLayer.Repositories.Repositories;

    public class HouseRepository:BaseRepository<House>
    {
        public HouseRepository(IRealtDbContext dataContext)
            : base(dataContext)
        {
        }
    }
}
