﻿namespace DataAccessLayer.Repositories.Repositories.DataBaseRepositories
{
    using DataAccessLayer.Models.Models;
    using DataAccessLayer.Repositories.Interfaces;
    using DataAccessLayer.Repositories.Repositories;

    public class FeaturesRepository : BaseRepository<Features>
    {
        public FeaturesRepository(IRealtDbContext dataContext)
            : base(dataContext)
        {
        }
    }
}
