﻿namespace DataAccessLayer.Repositories.Repositories.DataBaseRepositories
{
    using DataAccessLayer.Models.Models;
    using DataAccessLayer.Repositories.Interfaces;
    using DataAccessLayer.Repositories.Repositories;

    public class FlatRepository : BaseRepository<Flat>
    {
        public FlatRepository(IRealtDbContext dataContext)
            : base(dataContext)
        {
        }
    }
}
