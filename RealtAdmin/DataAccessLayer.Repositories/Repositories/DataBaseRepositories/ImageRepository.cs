﻿namespace DataAccessLayer.Repositories.Repositories.DataBaseRepositories
{
    using DataAccessLayer.Models.Models;
    using DataAccessLayer.Repositories.Interfaces;
    using DataAccessLayer.Repositories.Repositories;

    public class ImageRepository : BaseRepository<Image>
    {
        public ImageRepository(IRealtDbContext dataContext)
            : base(dataContext)
        {
        }
    }
}
