﻿namespace DataAccessLayer.Repositories.Repositories
{
    using System;

    using DataAccessLayer.Repositories.Interfaces;

    public class BaseSimpleRepository
    {
        public BaseSimpleRepository(IRealtDbContext dataContext)
        {
            this.Context = dataContext;
            if (this.Context == null)
            {
                throw new ArgumentException("Invalid context");
            }
        }

        protected IRealtDbContext Context
        {
            get;
            private set;
        }
    }
}
