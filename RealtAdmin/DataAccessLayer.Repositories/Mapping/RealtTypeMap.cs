﻿namespace DataAccessLayer.Repositories.Mapping
{
    using System.Data.Entity.ModelConfiguration;

    using DataAccessLayer.Models.Models;

    public class RealtTypeMap : EntityTypeConfiguration<RealtType>
    {
        public RealtTypeMap()
        {
            this.ToTable("RealtType");

            this.HasKey(p => p.Id);
            
            this.HasMany(p=>p.Houses).WithRequired(p=>p.RealtType).HasForeignKey(p=>p.IdRealtType);

            this.HasMany(p => p.Flats).WithRequired(p => p.RealtType).HasForeignKey(p => p.IdRealtType);
        }
    }
}
