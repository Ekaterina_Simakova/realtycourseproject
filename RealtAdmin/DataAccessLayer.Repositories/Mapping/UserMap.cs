﻿namespace DataAccessLayer.Repositories.Mapping
{
    using System.Data.Entity.ModelConfiguration;

    using DataAccessLayer.Models.Models;

    public class UserMap: EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            this.ToTable("Users", "Security");

            this.HasMany(p => p.Houses).WithRequired(p => p.User).HasForeignKey(p => p.IdUser);

            this.HasMany(p => p.Flats).WithRequired(p => p.User).HasForeignKey(p => p.IdUser);
        }
    }
}
