﻿namespace DataAccessLayer.Repositories.Mapping
{
    using System.Data.Entity.ModelConfiguration;

    using DataAccessLayer.Models.Models;

    public class FlatMap : EntityTypeConfiguration<Flat>
    {
        public FlatMap()
        {
            this.ToTable("Flat");   

            this.HasKey(p => p.Id);

            this.HasMany(p => p.Images).WithMany(p => p.Flats).Map(
                p =>
                {
                    p.MapLeftKey("FlatId");
                    p.MapRightKey("ImageId");
                    p.ToTable("Flat_Image");    
                });

            this.HasRequired(p => p.RealtType).WithMany(p => p.Flats).HasForeignKey(p => p.IdRealtType);

            this.HasRequired(p => p.User).WithMany(p => p.Flats).HasForeignKey(p => p.IdUser);
        }
    }
}
