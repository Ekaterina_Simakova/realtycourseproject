﻿namespace DataAccessLayer.Repositories.Mapping
{
    using System.Data.Entity.ModelConfiguration;

    using DataAccessLayer.Models.Models;

    public class FeaturesMap : EntityTypeConfiguration<Features>
    {
        public FeaturesMap()
        {
            this.ToTable("Features");

            this.HasKey(p => p.Id);

            this.Property(p => p.Id).IsRequired();

            this.Property(p => p.Name).HasMaxLength(60);

            this.HasMany(p => p.Houses).WithMany(p => p.Featureses).Map(
               p =>
               {
                   p.MapLeftKey("FeatureseId");
                   p.MapRightKey("HouseId");
                   p.ToTable("Featurese_House");
               });

            this.HasMany(p => p.Flats).WithMany(p => p.Featureses).Map(
               p =>
               {
                   p.MapLeftKey("FeatureseId");
                   p.MapRightKey("FlatId");
                   p.ToTable("Featurese_Flat"); 
               });
        }
    }
}
