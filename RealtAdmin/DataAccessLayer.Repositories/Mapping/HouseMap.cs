﻿namespace DataAccessLayer.Repositories.Mapping
{
    using System.Data.Entity.ModelConfiguration;

    using DataAccessLayer.Models.Models;

    public class HouseMap : EntityTypeConfiguration<House>
    {
        public HouseMap()
        {
            this.ToTable("House");

            this.HasKey(p => p.Id);
            
            this.HasMany(p => p.Images).WithMany(p => p.Houses).Map(
                p =>
                {
                    p.MapLeftKey("HouseId");
                    p.MapRightKey("ImageId");
                    p.ToTable("House_Image");
                });

            this.HasRequired(p => p.RealtType).WithMany(p => p.Houses).HasForeignKey(p => p.IdRealtType);

            this.HasRequired(p => p.User).WithMany(p => p.Houses).HasForeignKey(p => p.IdUser);  
        }
    }
}
