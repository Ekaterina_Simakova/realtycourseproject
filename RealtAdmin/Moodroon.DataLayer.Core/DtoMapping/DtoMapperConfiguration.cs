﻿using AutoMapper;
using DataAccessLayer.Models.Models;
using Moodroon.DataLayer.Core.DTO;

namespace Moodroon.DataLayer.Core.DtoMapping
{
   
    public class DtoMapperConfiguration
    {
        private static IMapper _mapperConfiguration;


        public static IMapper Instance
        {
            get
            {
                if (_mapperConfiguration == null)
                {
                    lock (typeof(IMapper))
                    {
                        if (_mapperConfiguration == null)
                            _mapperConfiguration = GetMapper();
                    }
                }

                return _mapperConfiguration;
            }
        }

        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Features, FeaturesDto>();
                cfg.CreateMap<FeaturesDto, Features>();
                cfg.CreateMap<Flat, FlatDto>();
                cfg.CreateMap<FlatDto, Flat>();
                cfg.CreateMap<House, HouseDto>();
                cfg.CreateMap<HouseDto, House>();
                cfg.CreateMap<Image, ImageDto>();
                cfg.CreateMap<ImageDto, Image>();
                cfg.CreateMap<RealtType, RealtTypeDto>();
                cfg.CreateMap<RealtTypeDto, RealtType>();
                cfg.CreateMap<User, UserDto>();
                cfg.CreateMap<UserDto, User>();
            });
            return config.CreateMapper();
        }
    }
}
