﻿namespace Moodroon.DataLayer.Core.DTO
{
    using System.Collections.Generic;

    public class ImageDto
    {
        public int Id { get; set; }

        public string Url { get; set; }     
    }
}
