﻿namespace Moodroon.DataLayer.Core.DTO
{
    using System.Collections.Generic;

    public class FeaturesDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
