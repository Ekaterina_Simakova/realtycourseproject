﻿namespace Moodroon.DataLayer.Core.DTO
{
    using System.Collections.Generic;

    public class HouseDto
    {
        public int Id { get; set; }

        public int CountRooms { get; set; }

        public int TotalSquare { get; set; }

        public string Adress { get; set; }

        public string Description { get; set; }

        public RealtTypeDto RealtType { get; set; }

        public int Price { get; set; }

        public UserDto User { get; set; }

        public List<ImageDto> ImageDtos { get; set; }

        public List<FeaturesDto> FeaturesDtos { get; set; }
    }
}
