﻿namespace Moodroon.DataLayer.Core.DTO
{
    using System.Collections.Generic;

    public class RealtTypeDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
