﻿using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.Models.Models;
using DataAccessLayer.Repositories.Repositories.DataBaseRepositories;
using Microsoft.Practices.Unity;
using Moodroon.DataLayer.Core.DTO;

namespace Moodroon.DataLayer.Core.Services
{
    public class FlatService : BaseApplicationService
    {
        public FlatService(IUnityContainer container) : base(container)
        {
        }

        public List<FlatDto> GetFlats()
        {
            return this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FlatRepository>();
                var result = repo.Get().ToList();
                var res = result.Select(p => new FlatDto
                {
                    Id = p.Id,
                    RealtType = this.Mapper.Map<RealtType, RealtTypeDto>(p.RealtType),
                    Adress = p.Adress,
                    CountRooms = p.CountRooms,
                    Description = p.Description,
                    Price = p.Price,
                    TotalSquare = p.TotalSquare,
                    User = this.Mapper.Map<User, UserDto>(p.User),
                    FeaturesDtos = p.Featureses.Select(x => this.Mapper.Map<Features, FeaturesDto>(x)).ToList(),
                    ImageDtos = p.Images.Select(x => this.Mapper.Map<Image, ImageDto>(x)).ToList()

                }).ToList();
                return res;
            });
        }

        public void AddFlat(FlatDto dto)
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FlatRepository>();
                repo.Add(new Flat
                {
                    TotalSquare = dto.TotalSquare,
                    Adress = dto.Adress,
                    CountRooms = dto.CountRooms,
                    Description = dto.Description,
                    Price = dto.Price,
                    RealtType = uow.GetRepository<RealTypeRepository>().GetSingle(p => p.Id == dto.RealtType.Id),
                    User = uow.GetRepository<UserRepository>().GetSingle(p => p.UserName.Equals(dto.User.UserName))
                });
                uow.SaveChanges();
            });
        }

        public void EditFlat(FlatDto dto)
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FlatRepository>();
                var dataItem = repo.GetSingle(p => p.Id.Equals(dto.Id));
                dataItem.Adress = dto.Adress;
                dataItem.CountRooms = dto.CountRooms;
                dataItem.Description = dto.Description;
                dataItem.Price = dto.Price;
                dataItem.TotalSquare = dto.TotalSquare;
                dataItem.IdRealtType = this.Mapper.Map<RealtTypeDto, RealtType>(dto.RealtType).Id;
                dataItem.IdUser = uow.GetRepository<UserRepository>().GetSingle(p => p.UserName.Equals(dto.User.UserName)).Id;
                repo.Modify(dataItem);
                uow.SaveChanges();
            });
        }

        public void DeleteFlat(int id)
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FlatRepository>();
                var delObj = repo.GetSingle(p => p.Id.Equals(id));
                repo.Remove(delObj);
                uow.SaveChanges();
            });
        }

        public void AddIFeature(int id, int featureId)
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FlatRepository>();
                var flat = repo.GetSingle(p => p.Id.Equals(id));
                flat.Featureses.Add(uow.GetRepository<FeaturesRepository>().GetSingle(p=>p.Id== featureId));
                repo.Modify(flat);
                uow.SaveChanges();
            });
        }

        public void AddImage(int id, string url)
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FlatRepository>();
                var flat = repo.GetSingle(p => p.Id.Equals(id));
                flat.Images.Add(new Image { Url = url });
                repo.Modify(flat);
                uow.SaveChanges();
            });
        }

        public void DeleteImage(int id, string url) 
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FlatRepository>();
                var flat = repo.GetSingle(p => p.Id.Equals(id));
                var image = flat.Images.FirstOrDefault(p => p.Url.Equals(url));
                flat.Images.Remove(image);
                repo.Modify(flat);
                uow.SaveChanges();
            });
        }
    }

}

