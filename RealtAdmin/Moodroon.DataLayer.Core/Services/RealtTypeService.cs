﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DataAccessLayer.Models.Models;
using DataAccessLayer.Repositories.Repositories.DataBaseRepositories;
using Microsoft.Practices.Unity;
using Moodroon.DataLayer.Core.DTO;

namespace Moodroon.DataLayer.Core.Services
{
    public class RealtTypeService : BaseApplicationService
    {
        public RealtTypeService(IUnityContainer container) : base(container)
        {
        }

        public List<RealtTypeDto> GetRealtTypes()
        {
            return this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<RealTypeRepository>();
                return repo.Get().Select(p => this.Mapper.Map<RealtType, RealtTypeDto>(p)).ToList();
            });
        }

        public bool AddRealt(RealtTypeDto dto)  
        {
            return this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<RealTypeRepository>();
                if (repo.Get().Any(p => p.Name.Equals(dto.Name, StringComparison.CurrentCultureIgnoreCase)))
                {
                    return false;
                }
                repo.Add(this.Mapper.Map<RealtTypeDto, RealtType>(dto));
                uow.SaveChanges();
                return true;
            });
        }

        public bool EditRealt(RealtTypeDto dto)
        {
            return this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<RealTypeRepository>();
                var obj = repo.GetSingle(p => p.Id == dto.Id);
                if (!obj.Name.Equals(dto.Name) && repo.Get().Any(p => p.Name.Equals(dto.Name, StringComparison.CurrentCultureIgnoreCase)))
                {
                    return false;
                }
                obj.Name = dto.Name;
                uow.SaveChanges();
                return true;
            });
        }

        public void DeleteRealt(int id) 
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<RealTypeRepository>();
                var del = repo.GetSingle(p => p.Id == id);
                repo.Remove(del);
                uow.SaveChanges();
            });
        }
    }
}
