﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.Models.Models;
using DataAccessLayer.Repositories.Repositories.DataBaseRepositories;
using Moodroon.DataLayer.Core.DTO;

namespace Moodroon.DataLayer.Core.Services
{
    using Microsoft.Practices.Unity;

    public class FeaturesService : BaseApplicationService
    {
        public FeaturesService(IUnityContainer container)
            : base(container)
        {
        }

        public List<FeaturesDto> GetFeatures()
        {
            return this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FeaturesRepository>();
                return repo.Get().ToList().Select(p => this.Mapper.Map<Features, FeaturesDto>(p)).ToList();
            });
        }

        public bool AddFeature(FeaturesDto dto)
        {
            return this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FeaturesRepository>();
                if (repo.Get().Any(p => p.Name.Equals(dto.Name, StringComparison.CurrentCultureIgnoreCase)))
                {
                    return false;
                }
                repo.Add(this.Mapper.Map<FeaturesDto,Features>(dto));
                uow.SaveChanges();
                return true;
            });
        }

        public bool EditFeature(FeaturesDto dto)    
        {
            return this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FeaturesRepository>();
                var obj=repo.GetSingle(p=>p.Id==dto.Id);
                if (!obj.Name.Equals(dto.Name) && repo.Get().Any(p => p.Name.Equals(dto.Name, StringComparison.CurrentCultureIgnoreCase)))
                {
                    return false;
                }
                obj.Name = dto.Name;
                uow.SaveChanges();
                return true;
            });
        }

        public void DeleteFeature(int id)    
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FeaturesRepository>();
                var del = repo.GetSingle(p => p.Id == id);
                repo.Remove(del);
                uow.SaveChanges();
            });
        }

        public List<FeaturesDto> GetFlatFeatures(int id)
        {
            return this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FeaturesRepository>();
                return repo.Get(p => !p.Flats.Select(x => x.Id).Contains(id))
                .ToList()
                .Select(p => this.Mapper.Map<Features, FeaturesDto>(p))
                .ToList();
            });
        }

        public void DeleteFlatFeature(int flatId, int featureId)
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FeaturesRepository>();
                var feature = repo.GetSingle(p => p.Id == featureId);
                var flatRepo = uow.GetRepository<FlatRepository>();
                var flat = flatRepo.GetSingle(p => p.Id == flatId);
                flat.Featureses.Remove(feature);
                flatRepo.Modify(flat);
                uow.SaveChanges();
            });
        }

        public List<FeaturesDto> GetHouseFeatures(int id)   
        {
            return this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FeaturesRepository>();
                return repo.Get(p => !p.Houses.Select(x => x.Id).Contains(id))
                .ToList()
                .Select(p => this.Mapper.Map<Features, FeaturesDto>(p))
                .ToList();
            });
        }

        public void DeleteHouseFeature(int houseId, int featureId)      
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<FeaturesRepository>();
                var feature = repo.GetSingle(p => p.Id == featureId);
                var flatRepo = uow.GetRepository<HouseRepository>();
                var flat = flatRepo.GetSingle(p => p.Id == houseId);
                flat.Featureses.Remove(feature);
                flatRepo.Modify(flat);
                uow.SaveChanges();
            });
        }


    }
}
