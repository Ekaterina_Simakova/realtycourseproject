﻿using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.Models.Models;
using DataAccessLayer.Repositories.Repositories.DataBaseRepositories;
using Microsoft.Practices.Unity;
using Moodroon.DataLayer.Core.DTO;

namespace Moodroon.DataLayer.Core.Services
{
    public class HouseService : BaseApplicationService
    {
        public HouseService(IUnityContainer container) : base(container)
        {
        }

        public List<HouseDto> GetHouses()
        {
            return this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<HouseRepository>();
                var result = repo.Get().ToList();
                var res = result.Select(p => new HouseDto
                {
                    Id = p.Id,
                    RealtType = this.Mapper.Map<RealtType, RealtTypeDto>(p.RealtType),
                    Adress = p.Adress,
                    CountRooms = p.CountRooms,
                    Description = p.Description,
                    Price = p.Price,
                    TotalSquare = p.TotalSquare,
                    User = this.Mapper.Map<User, UserDto>(p.User),
                    FeaturesDtos = p.Featureses.Select(x => this.Mapper.Map<Features, FeaturesDto>(x)).ToList(),
                    ImageDtos = p.Images.Select(x => this.Mapper.Map<Image, ImageDto>(x)).ToList()

                }).ToList();
                return res;
            });
        }

        public void AddHouse(HouseDto dto)
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<HouseRepository>();
                repo.Add(new House
                {
                    TotalSquare = dto.TotalSquare,
                    Adress = dto.Adress,
                    CountRooms = dto.CountRooms,
                    Description = dto.Description,
                    Price = dto.Price,
                    RealtType = uow.GetRepository<RealTypeRepository>().GetSingle(p => p.Id == dto.RealtType.Id),
                    User = uow.GetRepository<UserRepository>().GetSingle(p => p.UserName.Equals(dto.User.UserName))
                });
                uow.SaveChanges();
            });
        }

        public void AddUserHouse(HouseDto dto, int userId)
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<HouseRepository>();
                repo.Add(new House
                {
                    TotalSquare = dto.TotalSquare,
                    Adress = dto.Adress,
                    CountRooms = dto.CountRooms,
                    Description = dto.Description,
                    Price = dto.Price,
                    RealtType = uow.GetRepository<RealTypeRepository>().GetSingle(p => p.Id == dto.RealtType.Id),
                    IdUser = userId
                });
                uow.SaveChanges();
            });
        }

        public void EditHouse(HouseDto dto)
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<HouseRepository>();
                var dataItem = repo.GetSingle(p => p.Id.Equals(dto.Id));
                dataItem.Adress = dto.Adress;
                dataItem.CountRooms = dto.CountRooms;
                dataItem.Description = dto.Description;
                dataItem.Price = dto.Price;
                dataItem.TotalSquare = dto.TotalSquare;
                dataItem.IdRealtType = this.Mapper.Map<RealtTypeDto, RealtType>(dto.RealtType).Id;
                dataItem.IdUser = uow.GetRepository<UserRepository>().GetSingle(p => p.UserName.Equals(dto.User.UserName)).Id;
                repo.Modify(dataItem);
                uow.SaveChanges();
            });
        }

        public void DeleteHouse(int id)
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<HouseRepository>();
                var delObj = repo.GetSingle(p => p.Id.Equals(id));
                repo.Remove(delObj);
                uow.SaveChanges();
            });
        }

        public void AddIFeature(int id, int featureId)
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<HouseRepository>();
                var house = repo.GetSingle(p => p.Id.Equals(id));
                house.Featureses.Add(uow.GetRepository<FeaturesRepository>().GetSingle(p => p.Id == featureId));
                repo.Modify(house);
                uow.SaveChanges();
            });
        }

        public void AddImage(int id, string url)
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<HouseRepository>();
                var house = repo.GetSingle(p => p.Id.Equals(id));
                house.Images.Add(new Image { Url = url });
                repo.Modify(house);
                uow.SaveChanges();
            });
        }

        public void DeleteImage(int id, string url)
        {
            this.InvokeInUnitOfWorkScope(uow =>
            {
                var repo = uow.GetRepository<HouseRepository>();
                var house = repo.GetSingle(p => p.Id.Equals(id));
                var image = house.Images.FirstOrDefault(p => p.Url.Equals(url));
                house.Images.Remove(image);
                repo.Modify(house);
                uow.SaveChanges();
            });
        }
    }

}

